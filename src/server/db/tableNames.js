const tableNames = [
	'character',
	'characterList',
	'stash',
	'login',
	'leaderboard',
	'customMap',
	'customChannels',
	'error',
	'modLog',
	'accountInfo',
	'recipes'
];

module.exports = tableNames;
